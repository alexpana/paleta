local json = require("json")

local M = { image_name = nil, palette = { { tags = {}, colors = {} } }, current_index = 1 }

function M.set_image_name(image_name)
    M.image_name = image_name
end

function M.set_index(index)
    M.current_index = index
    if M.palette[index] == nil then M.palette[index] = {} end

    M.palette[M.current_index].tags = {}
end

function M.set_colors(colors)
    M.palette[M.current_index].colors = colors
    M.save()
end

function M.save()
    local f = io.open(M.image_name .. ".json", "w")
    f:write(json.encode(M.palette))
    io.close(f)
end

return M
