local color = require('color')
local style = {}

SCALE = 1

style.font_bold = love.graphics.newFont("data/TAHOMABD.TTF", 12)
style.font = love.graphics.newFont("data/TAHOMA.TTF", 11)
style.font_label = love.graphics.newFont("data/TAHOMABD.TTF", 11)

style.foreground = {color.str2rgb "#cccccc" }
style.foreground_header = { color.str2rgb "#E0E0E0" }
style.foreground_statusbar = { color.str2rgb "#a0a0a0" }
style.foreground_label = { color.str2rgb "#dddddd" }

style.background = { color.str2rgb "#333333" }
style.background_header = { color.str2rgb "#2B2B2B" }
style.background_editor = { color.str2rgb "#202020" }

style.separator = { color.str2rgb "#0D0D0D" }

style.header_height = 24

return style
