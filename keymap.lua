local command = require('command')

local keymap = {}
keymap.modkeys = {}
keymap.commands = {}

-- https://love2d.org/wiki/KeyConstant
local modkey_map = {
    ["lctrl"] = "ctrl",
    ["lalt"] = "alt",
    ["lshift"] = "shift",
    ["rctrl"] = "ctrl",
    ["ralt"] = "alt",
    ["rshift"] = "shift",
}

function compose_chord(key)
    if modkey_map[key] ~= nil then return "" end

    local chord = ""
    if love.keyboard.isDown("lctrl") or love.keyboard.isDown("rctrl") then chord = chord .. "ctrl+" end
    if love.keyboard.isDown("lshift") or love.keyboard.isDown("rshift") then chord = chord .. "shift+" end
    if love.keyboard.isDown("lalt") or love.keyboard.isDown("ralt") then chord = chord .. "alt+" end

    chord = chord .. key
    return chord
end

function keymap.keypressed(key)
    local chord = compose_chord(key)

    local command_name = keymap.commands[chord]
    if command_name ~= nil then 
        command.perform(command_name) 
    end
end

function keymap.keyreleased(key)
end

function keymap.add(commands)
    keymap.commands = commands
end

keymap.add({
    ['1'] = 'palette:select_1',
    ['2'] = 'palette:select_2',
    ['3'] = 'palette:select_3',
    ['4'] = 'palette:select_4',
    ['5'] = 'palette:select_5',
    ['6'] = 'palette:select_6',
    ['ctrl+z'] = 'undoredo:undo',
    ['ctrl+shift+z'] = 'undoredo:redo',
    ['ctrl+s'] = 'app:save',
})

return keymap
