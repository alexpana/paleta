local M = { callbacks = {} }

function M.register_callback(object, method)
    M.callbacks[#M.callbacks + 1] = { object, method }
end

function M.send(message_id, source, message)
    message.name = message_id
    message.src = source
    for i, callback in pairs(M.callbacks) do callback[2](callback[1], message) end
end

return M
