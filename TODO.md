# TODO

* [ ] Palette list item
* [ ] ItemList widget
* [ ] Better palette selection ui
* [ ] Better palette overlay (color rgb / hsv)
* [ ] Add buttons to ui library
* [ ] Color palette list
* [ ] Save project with embedded image (https://github.com/mlabbe/nativefiledialog)
* [x] Undo / redo for palette color picking
* [x] Swap colors in palette


## Nice to have

* [ ] Histograms
* [ ] Auto-generated palettes
* [ ] 2D palette
* [ ] Color wheel

## Research

* Research adobe lightroom lua sdk
* Research https://github.com/WetDesertRock/vivid/blob/master/vivid.lua