local Object = require("object")

local Rect = Object:extend("Rect")

function Rect:new(x, y, w, h)
    self.x = x
    self.y = y
    self.w = w
    self.h = h
end

function Rect.from_table(table)
    return Rect(table.x, table.y, table.w, table.h)
end

function Rect:expand(offset)
    self.x = self.x + offset.x
    self.y = self.y + offset.y
    self.w = self.w + offset.w
    self.h = self.h + offset.h

    return self
end

function Rect:inset(x, y, w, h)
    self.x = self.x + x
    self.y = self.y + y
    self.w = self.w - x - w
    self.h = self.h - y - h

    return self
end

function Rect:translate(x, y)
    self.x = self.x + x
    self.y = self.y + y
    return self
end


function Rect:copy()
    return Rect(self.x, self.y, self.w, self.h)
end

function Rect:unpack()
    return self.x, self.y, self.w, self.h
end

function Rect:contains(x, y)
    return self.x <= x and x <= self.x + self.w and self.y <= y and y <= self.y + self.h
end

function Rect:split_down(height, padding)
    local active = Rect(self.x, self.y + self.h - height, self.w, height)
    local next = Rect(self.x, self.y, self.w, self.h - height - padding)
    local split = Rect(next.x, next.h, next.w, padding)
    return active, next, split
end

function Rect:split_up(height, padding)
    local active = Rect(self.x, self.y, self.w, height)
    local next = Rect(self.x, self.y + height + padding, self.w, self.h - height - padding)
    local split = Rect(next.x, next.y - padding, next.w, padding)
    return active, next, split
end

function Rect:split_left(width, padding)
    local active = Rect(self.x, self.y, width, self.h)
    local next = Rect(self.x + width + padding, self.y, self.w - width - padding, self.h)
    local split = Rect(next.x - padding, next.y, padding, next.h)
    return active, next, split
end

function Rect:split_right(width, padding)
    local active = Rect(self.x + self.w - width, self.y, width, self.h)
    local next = Rect(self.x, self.y, self.w - width - padding, self.h)
    split = Rect(next.x + next.w, next.y, padding, next.h)
    return active, next, split
end

return Rect
