local Widget = require("ui.widget")

local ui = require("ui")
local style = require("style")

local PaletteListItem = Widget:extend("PaletteListItem")

function PaletteListItem:new(colors)
    self.super.new(self)

    self.palette = colors
    self.font = style.font_bold

    self.color = style.foreground
    self.background = style.background
    self.preferred_size = { 10000, 24 }
end

function PaletteListItem:draw()
    self.super.draw(self)

    local y = 0
    local my_height = self.region.h
    local cell_width = math.floor(self.region.w / self.palette:size())

    for i, col in pairs(self.palette:colors()) do
        PaletteListItem.draw_color(self.region.x + (i - 1) * cell_width, self.region.y, cell_width, my_height, col)
    end
end

function PaletteListItem.draw_color(x, y, w, h, color)
    love.graphics.setColor(color)
    love.graphics.rectangle("fill", x, y, w, h)
end

return Label
