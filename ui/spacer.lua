local Widget = require("ui.widget")

local Spacer = Widget:extend("Spacer")

function Spacer:new(size)
    self.super.new(self)
    self.preferred_size = { size, size }
end

return Spacer
