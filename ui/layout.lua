local Object = require("object")

local ui = require("ui")

local Layout = Object:extend("Layout")

function Layout:new()
    self.widgets = {}
end

function Layout:add_widget(widget)
    self.widgets[#self.widgets + 1] = widget
end

function Layout:get_widgets()
    local i = 0
    return function()
        i = i + 1;
        return self.widgets[i]
    end
end

function Layout:set_region(region)
    self.region = region
end

function Layout:draw()
    for widget in self:get_widgets() do widget:draw() end
end

function Layout:update()
    for widget in self:get_widgets() do widget:update() end
end

return Layout
