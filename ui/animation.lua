local M = {}

function M.lerp_to(from, target, rate)
    if math.abs(from - target) <= rate then return target end

    if from > target then rate = rate * -1 end
    return from + rate
end

return M
