local Layout = require("ui.layout")
local Rect = require("rect")

local VerticalLayout = Layout:extend("VerticalLayout")

function VerticalLayout:new()
    self.super.new(self)

    self.spacing = 2
    self.insets = Rect(0, 0, 0, 0)
    self.expand_factor = {}
end

function VerticalLayout:add_widget(widget, expand)
    self.super.add_widget(self, widget)

    if expand ~= nil then
        self.expand_factor[#self.widgets] = expand
    else
        self.expand_factor[#self.widgets] = 0
    end
end

function VerticalLayout:set_region(region)
    self.super.set_region(self, region)
    self:do_layout()
end

function VerticalLayout:do_layout()
    local widget_required_height = 0
    local widget_expand_units = 0
    local widget_index = 1
    for widget in self:get_widgets() do
        local ww, wh = widget:get_preferred_size()
        widget_required_height = widget_required_height + wh
        widget_expand_units = self.expand_factor[widget_index]
        widget_index = widget_index + 1
    end

    local remaining_height = self.region.h - widget_required_height
    local unit_expand_height = remaining_height / widget_expand_units

    local offset = 0
    widget_index = 1
    for widget in self:get_widgets() do
        local expand_height = self.expand_factor[widget_index] * unit_expand_height
        local ww, wh = widget:get_preferred_size()
        widget:set_region(Rect(self.region.x + self.insets.x, self.region.y + offset + self.insets.y,
                               self.region.w - self.insets.x - self.insets.w, wh + expand_height))

        offset = offset + wh + self.spacing
        widget_index = widget_index + 1
    end
end

return VerticalLayout
