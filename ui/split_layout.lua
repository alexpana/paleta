local style = require("style")
local bit = require("bit")

local Object = require("object")
local Rect = require("rect")
local Layout = require("ui.layout")

local Split = Object:extend("Split")

function Split:new(direction, size)
    self.direction = direction
    self.size = size

    self.widget = nil
    self.next_split = nil
    self.region = { 0, 0, 0, 0 }
    self.visible = true
end

local SplitLayout = Layout:extend("SplitLayout")

SplitLayout.RESIZABLE = 1
SplitLayout.HIDDEN = 2

local SPLIT_LEFT = 1
local SPLIT_RIGHT = 2
local SPLIT_DOWN = 3
local SPLIT_UP = 4
local SPLIT_FILL = 5

function SplitLayout:new()
    self.root_split = Split()

    self.current_split = self.root_split

    self.splits = { self.root_split }
    self.split_size = 1
    self.should_draw_splits = false

    self.region = {}
    self.active_rect = { 0, 0, 0, 0 }
    self.next_rect = { 0, 0, 0, 0 }
end

function SplitLayout:split_down(height, widget, flags)
    self:split(SPLIT_DOWN, height, widget, flags)
end

function SplitLayout:split_up(height, widget, flags)
    self:split(SPLIT_UP, height, widget, flags)
end

function SplitLayout:split_left(width, widget, flags)
    self:split(SPLIT_LEFT, width, widget, flags)
end

function SplitLayout:split_right(width, widget, flags)
    self:split(SPLIT_RIGHT, width, widget, flags)
end

function SplitLayout:split_fill(widget)
    self.current_split.direction = SPLIT_FILL
    self.current_split.widget = widget
    self.current_split.next_split = nil
    self.current_split = nil
end

function SplitLayout:split(direction, size, widget, flags)
    flags = flags or 0

    self.current_split.direction = direction
    self.current_split.size = size
    self.current_split.widget = widget
    self.current_split.visible = bit.band(flags, SplitLayout.HIDDEN) == 0

    self.current_split.next_split = Split()
    self.current_split = self.current_split.next_split
    self.splits[#self.splits + 1] = self.current_split
end

function SplitLayout:get_widgets()
    local split = self.root_split
    local index = 0
    return function()
        if split ~= nil and split.widget ~= nil then
            local this_widget = split.widget
            split = split.next_split
            index = index + 1
            return this_widget
        end
    end
end

function SplitLayout:set_region(region)
    local r = Rect.from_table(region)
    local split = self.root_split

    local active, next, split_region

    while split ~= nil do
        if split.direction == SPLIT_LEFT then
            active, next, split_region = r:split_left(split.size, self.split_size)
        elseif split.direction == SPLIT_RIGHT then
            active, next, split_region = r:split_right(split.size, self.split_size)
        elseif split.direction == SPLIT_UP then
            active, next, split_region = r:split_up(split.size, self.split_size)
        elseif split.direction == SPLIT_DOWN then
            active, next, split_region = r:split_down(split.size, self.split_size)
        elseif split.direction == SPLIT_FILL then
            active = r
            next = nil
            split_region = nil
        end

        r = next
        split.widget:set_region(active)
        split.region = split_region

        split = split.next_split
    end
end

function SplitLayout:draw()
    self.super.draw(self)
    self:draw_splits()
end

function SplitLayout:draw_splits()
    for i, split in ipairs(self.splits) do
        local r = split.region
        if r ~= nil and split.visible then
            r = r:copy()
            if r.w < r.h then
                r.y = r.y + 1
                r.h = r.h - 2
            else
                r.x = r.x + 1
                r.w = r.w - 2
            end
            love.graphics.setColor(style.separator)
            love.graphics.rectangle("fill", r:unpack())
        end
    end
end

return SplitLayout
