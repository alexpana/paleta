local Object = require("Object")

local Sprite = Object:extend()

function Sprite:new(image, quad)
    self.image = image
    self.quad = quad
end

function Sprite:draw(x, y)
    love.graphics.draw(self.image, self.quad, x, y)
end

function Sprite:get_size()
    return self.quad:getViewport()
end

return Sprite