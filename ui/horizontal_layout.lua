local Layout = require("ui.layout")
local Rect = require("rect")

local HorizontalLayout = Layout:extend("HorizontalLayout")

--- HorizontalLayout constructor.
-- This layout will distribute its child widgets from left to right.
-- Each widget can have an expand factor. All remaining empty space
-- of the layout will be distributed to widgets according to their
-- expand factors.
function HorizontalLayout:new()
    self.super.new(self)

    self.spacing = 2
    self.insets = Rect(0, 0, 0, 0)
    self.expand_factor = {}
end

--- Add a widget to the layout.
-- @param widget the widget to be added
-- @param expand optional expand scale for the widget
function HorizontalLayout:add_widget(widget, expand)
    self.super.add_widget(self, widget)
    self.expand_factor[#self.widgets] = 0

    if expand ~= nil then self.expand_factor[#self.widgets] = expand end
end

-- @see Widget:set_region
function HorizontalLayout:set_region(region)
    self.super.set_region(self, region)
    self:do_layout()
end

function HorizontalLayout:do_layout()
    local widget_required_width = 0
    local widget_expand_units = 0
    local widget_index = 1
    for widget in self:get_widgets() do
        local ww, wh = widget:get_preferred_size()
        widget_required_width = widget_required_width + ww
        widget_expand_units = self.expand_factor[widget_index]
        widget_index = widget_index + 1
    end

    local remaining_width = self.region.w - widget_required_width - self.insets.x - self.insets.w
    local unit_expand_width = remaining_width / widget_expand_units

    local offset = 0
    widget_index = 1
    for widget in self:get_widgets() do
        local expand_width = self.expand_factor[widget_index] * unit_expand_width
        local ww, wh = widget:get_preferred_size()
        widget:set_region(Rect(self.region.x + self.insets.x + offset, self.region.y + self.insets.y, ww + expand_width,
                               self.region.h - self.insets.y - self.insets.h))

        offset = offset + ww + self.spacing
        widget_index = widget_index + 1
    end
end

return HorizontalLayout
