local Widget = require("ui.widget")

local ui = require("ui")
local style = require("style")

local Label = Widget:extend("Label")

function Label:new(text)
    self.super.new(self)

    self.text = text
    self.font = style.font_bold

    self.color = style.foreground
    self.background = style.background

    self.text_position = { 1, 6 }
    self.preferred_size = { 10000, 24 }
end

function Label:set_region(region)
    self.super.set_region(self, region)
    self:compute_text_position()
end

function Label:compute_text_position()
end

function Label:draw()
    love.graphics.setColor(self.background)
    love.graphics.rectangle("fill", self.region.x + 1, self.region.y + 1, self.region.w - 2, self.region.h - 2)

    love.graphics.setColor(self.color)
    love.graphics.setFont(self.font)
    love.graphics.print(self.text, self.region.x + self.text_position[1], self.region.y + self.text_position[2])
end

return Label
