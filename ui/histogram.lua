local Widget = require("ui.widget")

local style = require("style")

local Histogram = Widget:extend("Histogram")

function Histogram:new(histogram_fn)
    self.super.new(self)
    self.histogram_fn = histogram_fn
    self.preferred_size = { 10000, 100 }
end

function Histogram:draw()
    love.graphics.setColor(style.background_editor)
    love.graphics.rectangle("fill", self.region.x, self.region.y, self.region.w, self.region.h + 2)

    love.graphics.push()
    love.graphics.translate(self.region.x, self.region.y)

    love.graphics.setColor(style.background_editor)
    love.graphics.rectangle("fill", 0, 0, 255 + 2, self.region.h + 2)

    for x = 1, self.region.w - 2 do
        local value, color = self.histogram_fn(x / self.region.w)
        local height = value * self.region.h

        love.graphics.setColor(color)
        love.graphics.rectangle("fill", x, self.region.h - height + 1, 1, height)
    end

    love.graphics.pop()
end

return Histogram
