local style = require("style")

local Widget = require("ui.widget")
local HorizontalLayout = require("ui.horizontal_layout")
local VerticalLayout = require("ui.vertical_layout")
local Label = require("ui.label")
local Rect = require("rect")

local DockPanel = Widget:extend("DockPanel")

function DockPanel:new(label, widget)
    self.super.new(self)

    self.label = label
    self.widget = widget

    self.layout = VerticalLayout()
    self.layout.spacing = 0

    -- Create widgets
    self.header = Widget()
    self.header.preferred_size[2] = style.header_height
    self.header.background = style.background_header
    self.header.layout = HorizontalLayout()
    self.header.layout.insets = Rect(2, 0, 1, 1)

    self.header_label = Label(label)
    self.header_label.background = style.background_header

    -- Layout widgets
    self.header.layout:add_widget(self.header_label, 1)

    self.layout:add_widget(self.header)
    self.layout:add_widget(self.widget, 1)
end

function DockPanel:set_region(region)
    self.super.set_region(self, region)
    -- self.widget:set_region(region:copy():inset(1, style.header_height + 1, 1, 1))
end

-- function DockPanel:draw()
--     self.super.draw(self)

--     local header_rect = self.region:copy():inset(1, 1, 1, 1)
--     header_rect.h = style.header_height

--     love.graphics.setColor(style.background_header)
--     love.graphics.rectangle("fill", header_rect:unpack())

--     love.graphics.setColor(style.foreground_header)
--     love.graphics.setFont(style.font_bold)
--     love.graphics.print(self.label, self.region.x + 7, self.region.y + 6)

--     self.widget:draw()
-- end

return DockPanel
