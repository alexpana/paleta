local Object = require("object")
local Rect = require("rect")
local Layout = require("ui.layout")

local ui = require("ui")
local style = require("style")

local Widget = Object:extend("Widget")

function Widget:new()
    self.layout = Layout()
    self.region = Rect(0, 0, 0, 0)
    self.has_mouse_hover = false
    self.padding = Rect(0, 0, 0, 0)
    self.color = { 1.0, 1.0, 1.0, 1.0 }
    self.background = style.background
    self.preferred_size = { -1, -1 }
end

function Widget:update()
    self.layout:update()

    local mp = ui.mouse_position

    if self.region == nil then debug() end

    if not self.has_mouse_hover and self.region:contains(mp[1], mp[2]) then
        self.has_mouse_hover = true
        self:on_mouse_enter()
    elseif self.has_mouse_hover and not self.region:contains(mp[1], mp[2]) then
        self.has_mouse_hover = false
        self:on_mouse_exit()
    elseif self.has_mouse_hover then
        self:on_mouse_move()
    end

    if self.has_mouse_hover and ui.mouse_down[1] then self:on_mouse_click() end
end

function Widget:set_background(color)
    self.background = color
    return self
end

function Widget:set_region(region)
    self.region = region
    self.layout:set_region(region)
    self.repaint()
end

function Widget:on_mouse_enter()
end

function Widget:on_mouse_exit()
end

function Widget:on_mouse_click()
end

function Widget:on_mouse_move()
end

function Widget:repaint()
end

function Widget:draw()
    love.graphics.setColor(unpack(self.background))
    love.graphics.rectangle("fill", self.region.x, self.region.y, self.region.w, self.region.h)

    self.layout:draw()
end

function Widget:get_preferred_size()
    if self.layout.get_preferred_size ~= nil then return self.layout.get_preferred_size end

    return self.preferred_size[1], self.preferred_size[2]
end

return Widget
