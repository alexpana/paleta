local style = require("style")
local Rect = require("rect")

local ui = {}

ui.next_rect = { 0, 0, 0, 0 } -- this rect will be split next
ui.active_rect = { 0, 0, 0, 0 } -- this rect is currently rendered
ui.splits = {}
ui.split_size = 1

ui.layout = {} -- main layout of the window

ui.repaint_requests = {}

ui.mouse_position = { 0, 0 }
ui.mouse_position_last_frame = { 0, 0 }
ui.mouse_down = { false, false, false }
ui.mouse_down_last_frame = { false, false, false }
ui.mouse_wheel = { 0, 0 }
-- ui.mouse_delta = {}

function ui.init()
    ui.next_rect = rect
    ui.active_rect = rect
    ui.splits = {}
end

function ui.update()
    ui.mouse_position_last_frame = ui.mouse_position
    ui.mouse_position = { love.mouse.getPosition() }

    ui.mouse_down_last_frame = { unpack(ui.mouse_down) }
    ui.mouse_down[1] = love.mouse.isDown(1)
    ui.mouse_down[2] = love.mouse.isDown(2)
    ui.mouse_down[3] = love.mouse.isDown(3)

    ui.layout:update()

    ui.mouse_wheel[1] = 0
    ui.mouse_wheel[2] = 0
end

function ui.wheelmoved(x, y)
    ui.mouse_wheel[1] = x
    ui.mouse_wheel[2] = y
end

function ui.draw()
    love.graphics.clear(unpack(style.background))
    ui.layout:draw()
end

function ui.resize(region)
    ui.layout:set_region(region)
end

-- input helper functions

function ui.is_mouse_pressed(button)
    return ui.mouse_down[button] and not ui.mouse_down_last_frame[button]
end

function ui.is_mouse_released(button)
    return not ui.mouse_down[button] and ui.mouse_down_last_frame[button]
end

function ui.is_mouse_down(button)
    return ui.mouse_down[button]
end

return ui
