local Widget = require("ui.widget")
local ui = require("ui")

local ImageButton = Widget:extend("ImageButton")

function ImageButton:new(sprite)
    self.super.new(self)

    self.sprite = sprite
    self.active_sprite = sprite

    self.hover_color = { 1.0, 1.0, 1.0, 0.6 }
    self.active_color = { 1.0, 1.0, 1.0, 1.0 }
    self.inactive_color = { 1.0, 1.0, 1.0, 0.3 }

    self.is_toggle = false

    self.is_active = false
    self.icon_pos = { 0, 0 }

    self.on_click_callback = function()
    end
end

function ImageButton:set_region(region)
    self.super.set_region(self, region)

    local sx, sy, sw, sh = self.sprite:get_size()
    self.icon_pos = { (region.w - sw) / 2, (region.h - sh) / 2 }

end

function ImageButton:draw()
    local draw_sprite = self.sprite
    if self.is_active then
        love.graphics.setColor(self.active_color)
        draw_sprite = self.active_sprite
    elseif self.has_mouse_hover then
        love.graphics.setColor(self.hover_color)
    else
        love.graphics.setColor(self.inactive_color)
    end
    draw_sprite:draw(self.region.x + self.icon_pos[1], self.region.y + self.icon_pos[2])
end

function ImageButton:update()
    self.super.update(self)

    if self.has_mouse_hover and ui.is_mouse_pressed(1) then
        if self.is_toggle then
            self.is_active = not self.is_active
        else
            self.is_active = true
        end

        if self.is_active then self.on_click_callback() end
    end

    if self.is_active and ui.is_mouse_released(1) and not self.is_toggle then self.is_active = false end
end

return ImageButton
