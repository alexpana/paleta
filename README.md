# Paleta

Lite app that creates color palettes from images to be used in Substance Designer materials.

![screenshot](screenshot.png)

## Misc

* Love uses luajit 5.1 with extensions: http://luajit.org/extensions.html
* `.luastyle` documentation: https://github.com/Koihik/LuaFormatter/blob/master/docs/Style-Config.md

## Code style

* `ClassName`, `variable_name`, `function_name`, `modulename`
* Documentation using [LDoc](https://github.com/lunarmodules/LDoc)
