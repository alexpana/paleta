local command = {}

command.map = {}

function command.perform(command_name)
    command.map[command_name]()
end

return command