local style = require("style")

local Object = require("object")

-- https://halt.software/dead-simple-layouts/

local RectCut = Object:extend("RectCut")

function RectCut:new(rect)
    self.rect = rect
end

function RectCut:bottom(height, padding)
    padding = padding == nil and 0 or padding

    local result = { self.rect.x, self.rect.y + self.rect.h - height, self.rect.w, height }
    self.rect = { self.rect.x, self.rect.y, self.rect.w, self.rect.h - height - padding }

    return result
end

function RectCut:top(height, padding)
    padding = padding == nil and 0 or padding

    local result = { self.rect.x, self.rect.y, self.rect.w, height }
    self.rect = { self.rect.x, self.rect.y + height + padding, self.rect.w, self.rect.h - height - padding}

    return result
end

function RectCut:left(width, padding)
    padding = padding == nil and 0 or padding

    local result = { self.rect.x, self.rect.y, width, self.rect.h }
    self.rect = { self.rect.x + width + padding, self.rect.y, self.rect.w - width - padding, self.rect.h }

    return result
end

function RectCut:right(width, padding)
    padding = padding == nil and 0 or padding

    local result = { self.rect.x + self.rect.w - width, self.rect.y, width, self.rect.h }
    self.rect = { self.rect.x, self.rect.y, self.rect.w - width - padding, self.rect.h }
    return result
end

return RectCut
