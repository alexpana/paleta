if os.getenv("LOCAL_LUA_DEBUGGER_VSCODE") == "1" then require("lldebugger").start() end

-- Classes
local Rect = require("rect")
local Color = require("color")
local ImageIndex = require("objects.image_data")
local RectCut = require("rectcut")

local DockPanel = require("ui.dock_panel")
local SplitLayout = require("ui.split_layout")

-- Views
local ImageView = require("views.image")
local PaletteView = require("views.palette")
local ModeView = require("views.modetab")
local PropertiesView = require("views.properties")
local StatusBar = require("views.statusbar")

-- modules
local style = require("style")
local ui = require("ui")
local keymap = require('keymap')
local command = require('command')
local undoredo = require("undoredo")
local struct = require("struct")
local ospath = require("ospath")
local icons = require("icons")
local library = require("library")

local msgbus = require("msgbus")

local statusbar_view
local image_view
local palette_view
local mode_view
local properties_view

function love.load()
    icons.load()

    statusbar_view = StatusBar()
    image_view = ImageView()
    palette_view = PaletteView(4)
    mode_view = ModeView()
    properties_view = PropertiesView()

    ui.layout = SplitLayout()
    local layout = ui.layout
    layout:split_down(25, statusbar_view)
    layout:split_left(50, mode_view)
    layout:split_right(300, DockPanel("Properties", properties_view))
    layout:split_down(150, DockPanel("Palette", palette_view))
    layout:split_fill(DockPanel("Image", image_view))

    ui.resize(Rect(0, 0, love.graphics.getWidth(), love.graphics.getHeight()))

    -- register undo / redo actions (TODO: move to each view)
    -- undoredo.register_action("image_view:pick_color", {
    --     function(color)
    --         local current_color = state.palette_view:get_color()

    --         print("image_view:pick_color:do(" .. Color.rgb2hex(unpack(image_view.selected_color)) .. " -> " ..
    --                   Color.rgb2hex(unpack(color)) .. ")")

    --         image_view.selected_color = color
    --         palette_view:set_color(color)

    --         return { current_color }
    --     end, function(color)
    --         print("image_view:pick_color:undo(" .. Color.rgb2hex(unpack(image_view.selected_color)) .. " -> " ..
    --                   Color.rgb2hex(unpack(color)) .. ")")
    --         image_view.selected_color = color
    --         palette_view:set_color(color)
    --     end,
    -- })

    -- undoredo.register_action("palette:set_index", {
    --     function(to)
    --         local current_index = palette_view.selected
    --         if current_index == to then return nil end
    --         print("palette:set_index:do(" .. current_index .. " -> " .. to .. ")")
    --         palette_view.selected = to
    --         return { current_index }
    --     end, function(to)
    --         local current_index = palette_view.selected
    --         print("palette:set_index:do(" .. current_index .. " -> " .. to .. ")")
    --         palette_view.selected = to
    --     end,
    -- })

    -- undoredo.register_action("palette:move_slot", {
    --     function(from, to)
    --         print("palette:move_slot:do(" .. from .. " -> " .. to .. ")")
    --         palette_view:move_index(from, to)
    --         palette_view.selected = to
    --     end, function(from, to)
    --         print("palette:move_slot:undo(" .. from .. " -> " .. to .. ")")
    --         palette_view:move_index(from, to)
    --         palette_view.selected = to
    --     end,
    -- })
end

function love.draw()
    -- love.graphics.clear(unpack(style.background))
    ui.draw()
end

function love.update(dt)
    ui.update(dt)
end

function love.wheelmoved(x, y)
    ui.wheelmoved(x, y)
end

function love.resize(w, h)
    ui.resize(Rect(0, 0, love.graphics.getWidth(), love.graphics.getHeight()))
end

function love.filedropped(file)
    local filename = file:getFilename()
    local base, ext = ospath.split_extension(filename)

    if ext == "paleta" then
        -- load binary save
        load(filename)
        save_path = filename
    else
        image_view:load_from_path(filename)
        save_path = base .. ".paleta"
    end

    os.execute("copy " .. filename .. " C:\\PaletaLibrary")
    library.set_image_name(filename)
    library.set_colors(1, palette_view.palette)
    library.save()
    -- image_data:indexImage(image_view.imagedata)
end
