local Object = {}

Object.__index = Object
Object.name = "Object"

function Object:new()
end

function Object:extend(name)
    local cls = {}

    -- metatable functions aren't searched recursively via __index
    for k, v in pairs(self) do if k:find("__") == 1 then cls[k] = v end end

    cls.__index = cls
    cls.super = self
    cls.name = name
    setmetatable(cls, self)
    return cls
end

function Object:__tostring()
    return self.name
end

function Object:__call(...)
    local obj = setmetatable({}, self)
    obj:new(...)
    return obj
end

return Object
