local Object = require("object")
local Color = require('color')

local ImageData = Object:extend("ImageData")

function histogram_peak(histogram)
    local mv = 0
    for k in pairs(histogram) do
        local v = histogram[k]
        if v > mv then mv = v end
    end
    return mv
end

function ImageData:new()
    self.indexed = false
    self.background_remove_threshold = 0.2
    self.samples_per_dimension = 128

    --  128 =   9520 unique /   1s
    --  256 =  35467 unique /   5s
    --  512 = 136484 unique /   28s
    -- 1024 = 362434 unique / 3m

    self.precision = 0
end

function ImageData:guessBackgroundColor()
    local guess_color = 0
    local guess_count = 0
    for color, count in pairs(self.colors) do
        if count > guess_count then
            guess_color = color
            guess_count = count
        end
    end

    return guess_color
end

function ImageData:indexImage(imagedata)
    local time_start = os.clock()

    self.data = imagedata
    self.hue_histogram = {}
    self.hue_histogram_peak = 0

    self.light_histogram = {}
    self.light_histogram_peak = 0

    self.colors = {}
    self.hsl = {}
    self.sample_count = 0

    local w = self.data:getWidth()
    local h = self.data:getHeight()

    local sample_radius_x = math.floor(w / self.samples_per_dimension)
    local sample_radius_y = math.floor(h / self.samples_per_dimension)

    for x = 0, w - sample_radius_x - 1, sample_radius_x do
        for y = 0, h - sample_radius_y - 1, sample_radius_y do
            local rx = love.math.random() * sample_radius_x
            local ry = love.math.random() * sample_radius_y
            local r, g, b = self.data:getPixel(x + rx, y + ry)

            local color = Color.rgb2hex(r, g, b)
            local col_hue, col_sat, col_val = Color.rgbhex2hsl(color)

            if self.colors[color] == nil then
                self.colors[color] = 1
                self.hsl[color] = { col_hue, col_sat, col_val }
            else
                self.colors[color] = self.colors[color] + 1
            end

            if self.hue_histogram[col_hue] == nil then
                self.hue_histogram[col_hue] = 1
            else
                self.hue_histogram[col_hue] = self.hue_histogram[col_hue] + 1
            end

            if self.light_histogram[col_val] == nil then
                self.light_histogram[col_val] = 1
            else
                self.light_histogram[col_val] = self.light_histogram[col_val] + 1
            end

            self.sample_count = self.sample_count + 1
        end
    end

    -- check if we want to remove the background color
    local background_color = self:guessBackgroundColor()

    local bg_count = self.colors[background_color]
    local bg_hue, bg_sat, bg_lig = Color.rgbhex2hsl(background_color)
    if bg_count > self.background_remove_threshold * self.sample_count then
        self.sample_count = self.sample_count - bg_count
        self.colors[background_color] = 0
        self.hue_histogram[bg_hue] = self.hue_histogram[bg_hue] - bg_count
        self.light_histogram[bg_lig] = self.light_histogram[bg_lig] - bg_count
    end

    self.hue_histogram_peak = histogram_peak(self.hue_histogram)
    self.light_histogram_peak = histogram_peak(self.light_histogram)

    -- for k in pairs(self.light_histogram) do print(k, self.light_histogram[k]) end

    local unique_colors = 0
    for k in pairs(self.colors) do unique_colors = unique_colors + 1 end

    self.indexed = true

    self.precision = self.sample_count / (w * h) * 100

    local time_end = os.clock()
    print(string.format("Elapsed time %f", time_end - time_start))
    print(string.format("Samples: %f", self.sample_count))
    print(string.format("Unique colors: %d", unique_colors))
    print(string.format("Precision: %2.2f", self.sample_count / (w * h) * 100))
end

function ImageData:generate_palette(base_color, strategy)
    local base = Color.rgb2hex(unpack(base_color))
    self.hsl[base] = { Color.rgbhex2hsl(base) }


    complementary_base = Color.rgb2hex(
        Color.hsl2rgb(
            Color.hueOffset(self.hsl[base][1], -180) / 360, self.hsl[base][2] / 255, self.hsl[base][3] / 255
        )
    )
    self.hsl[complementary_base] = { Color.rgbhex2hsl(complementary_base) }

    local thue = 10
    local tsat = 40
    local tval = 20

    local shadow = self:search_color(base, { -10, -70, -70, thue, tsat, tval })
    local highlight = self:search_color(base, { 5, 10, 30, thue, tsat, tval })
    local complementary_shadow = self:search_color(complementary_base, { 5, 10, 30, thue, tsat, tval })
    local complementary_highlight = self:search_color(complementary_base, { 5, 10, 30, thue, tsat, tval })

    return { 
        base_color, 
        { Color.hex2rgb(shadow) },
        { Color.hex2rgb(highlight) },
        { Color.hex2rgb(complementary_shadow) },
        { Color.hex2rgb(complementary_highlight) },
    }
end

function fitness(color, base, hsl, args)
    local hue_offset = args[1]
    local saturation_offset = args[2]
    local value_offset = args[3]

    local hue_threshold = args[4]
    local saturation_threshold = args[5]
    local value_threshold = args[6]

    return math.abs(hsl[base][1] + hue_offset - hsl[color][1]) < hue_threshold and
               math.abs(hsl[base][2] + saturation_offset - hsl[color][2]) < saturation_threshold and
               math.abs(hsl[base][3] + value_offset - hsl[color][3]) < value_threshold
end

function ImageData:search_color(base, args)
    local best = next(self.colors)

    local candidates = {}

    for color, count in pairs(self.colors) do
        if fitness(color, base, self.hsl, args) and self.colors[best] < self.colors[color] then best = color end
    end

    return best
end

return ImageData