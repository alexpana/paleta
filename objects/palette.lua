local color = require("color")
local Object = require("object")

local Palette = Object:extend("Palette")

function Palette:new(colors)
    self.colors = colors
end

function Palette:size()
    return #self.colors
end

function Palette:swap_colors(i1, i2)
    local tmp = self.colors[i1]
    self.colors[i1] = self.colors[i2]
    self.colors[i2] = tmp
end

function Palette:set_color(index, color)
    self.colors[index] = color
    self:print()
end

function Palette:serialize_binary()
    local bytes = ""
    bytes = bytes .. struct.pack("<L", #self.colors)
    for i = 1, #self.colors do bytes = bytes .. struct.pack("I", color.rgb2hex(unpack(self.colors[i]))) end
    return bytes
end

function Palette:deserialize_binary(data, offset)
    local color_count = struct.unpack("<L", data, offset)
    offset = offset + 8

    for i = 1, self.color_count do
        self.colors[i] = { color.hex2rgb(struct.unpack("<I", data, offset), 1) }
        offset = offset + 4
    end
    return offset
end

function Palette:print()
    print("{")
    for _, color in pairs(self.colors) do
        print(string.format("{%.6f, %.6f, %.6f},", color[1], color[2], color[3]))
    end
    print("}")
    io.stdout:flush()

end

return Palette