local M = {}

local Sprite = require("ui.sprite")

function M.load()
    local image = love.graphics.newImage("data/ui_icons.png")
    local iw, ih = image:getDimensions()
    M.edit_mode = Sprite(image, love.graphics.newQuad(0, 0, 40, 40, iw, ih))
    M.gallery_mode = Sprite(image, love.graphics.newQuad(40, 0, 40, 40, iw, ih))

    M.palette_hide_text = Sprite(image, love.graphics.newQuad(0, 40, 16, 16, iw, ih))
    M.palette_show_text = Sprite(image, love.graphics.newQuad(32, 40, 16, 16, iw, ih))
    M.palette_lock = Sprite(image, love.graphics.newQuad(48, 40, 16, 16, iw, ih))
    M.palette_unlock = Sprite(image, love.graphics.newQuad(16, 40, 16, 16, iw, ih))
end

return M
