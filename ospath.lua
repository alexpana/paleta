local M = {}

function M.split_extension(path)
    for i = string.len(path), 1, -1 do
        if path:byte(i) == 46 then return string.sub(path, 0, i - 1), string.sub(path, i + 1) end
    end
end

return M
