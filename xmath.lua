local M = {}

function M.clamp(x, min, max)
    return math.min(math.max(x, min), max)
end

return M