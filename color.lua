--
-- All rgb colors are 0..1
--
local Color = {}
local bit = require("bit")
local xmath = require("xmath")

function Color.hex2rgb(rgb)
    local r = bit.band(bit.rshift(rgb, 16), 0xff)
    local g = bit.band(bit.rshift(rgb, 8), 0xff)
    local b = bit.band(rgb, 0xff)
    return r / 255, g / 255, b / 255
end

function Color.hex2rgb(rgb, a)
    local r = bit.band(bit.rshift(rgb, 16), 0xff)
    local g = bit.band(bit.rshift(rgb, 8), 0xff)
    local b = bit.band(rgb, 0xff)
    return r / 255, g / 255, b / 255, a
end

function Color.rgb2hex(r, g, b)
    return bit.lshift(bit.band(r * 255, 0xff), 16) +
               bit.lshift(bit.band(g * 255, 0xff), 8) + bit.band(b * 255, 0xff)
end

function Color.str2hex(str)
    return Color.rgb2hex(tonumber(string.sub(str, 2, 3), 16) / 255,
                         tonumber(string.sub(str, 4, 5), 16) / 255,
                         tonumber(string.sub(str, 6, 7), 16) / 255)
end

function Color.str2rgb(str)
    return tonumber(string.sub(str, 2, 3), 16) / 255,
           tonumber(string.sub(str, 4, 5), 16) / 255,
           tonumber(string.sub(str, 6, 7), 16) / 255
end

function Color.hsl2rgb(h, s, l, a)
    if s <= 0 then return l, l, l, a end
    h, s, l = h * 6, s, l
    local c = (1 - math.abs(2 * l - 1)) * s
    local x = (1 - math.abs(h % 2 - 1)) * c
    local m, r, g, b = (l - .5 * c), 0, 0, 0

    if h < 1 then
        r, g, b = c, x, 0
    elseif h < 2 then
        r, g, b = x, c, 0
    elseif h < 3 then
        r, g, b = 0, c, x
    elseif h < 4 then
        r, g, b = 0, x, c
    elseif h < 5 then
        r, g, b = x, 0, c
    else
        r, g, b = c, 0, x
    end
    return (r + m), (g + m), (b + m), a
end

function Color.rgbhex2hsl(rgb)
    --r, g, b = r/255, g/255, b/255
    local r, g, b = Color.hex2rgb(rgb)

    local min = math.min(r, g, b)
    local max = math.max(r, g, b)
    local delta = max - min
    
    local h, s, l = 0, 0, ((min+max)/2)
    
    if l > 0 and l < 0.5 then s = delta/(max+min) end
    if l >= 0.5 and l < 1 then s = delta/(2-max-min) end
    
    if delta > 0 then
        if max == r and max ~= g then h = h + (g-b)/delta end
        if max == g and max ~= b then h = h + 2 + (b-r)/delta end
        if max == b and max ~= r then h = h + 4 + (r-g)/delta end
        h = h / 6;
    end
    
    if h < 0 then h = h + 1 end
    if h > 1 then h = h - 1 end
    
    return math.floor(h * 360), math.floor(s * 255), math.floor(l * 255)
end

function Color.hsl2rgb(h, s, l, a)
    if s <= 0 then return l, l, l, a end
    h, s, l = h * 6, s, l
    local c = (1 - math.abs(2 * l - 1)) * s
    local x = (1 - math.abs(h % 2 - 1)) * c
    local m, r, g, b = (l - .5 * c), 0, 0, 0

    if h < 1 then
        r, g, b = c, x, 0
    elseif h < 2 then
        r, g, b = x, c, 0
    elseif h < 3 then
        r, g, b = 0, c, x
    elseif h < 4 then
        r, g, b = 0, x, c
    elseif h < 5 then
        r, g, b = x, 0, c
    else
        r, g, b = c, 0, x
    end
    return (r + m), (g + m), (b + m), a
end

function Color.rgbhex2hue(rgb)
    local r = bit.band(bit.rshift(rgb, 16), 0xff)
    local g = bit.band(bit.rshift(rgb, 8), 0xff)
    local b = bit.band(rgb, 0xff)
    return Color.rgb2hue(r, g, b)
end

-- http://www.niwa.nu/2013/05/math-behind-colorspace-conversions-rgb-hsl/
function Color.rgb2hue(r, g, b)
    local min = math.min(math.min(r, g), b)
    local max = math.max(math.max(r, g), b)

    if min == max then return 0 end

    local hue = 0

    if r == max then hue = (g - b) / (max - min) end

    if g == max then hue = 2.0 + (b - r) / (max - min) end

    if b == max then hue = 4.0 + (r - g) / (max - min) end

    hue = hue * 60
    if hue < 0 then hue = hue + 360 end

    -- todo: this conversion is biased towards left hues
    return math.floor(hue)
end

function Color.hueOffset(color, offset)
    local result = color + offset

    if result > 360 then result = result - 360 end
    if result < 0 then result = result + 360 end

    return result
end

-- returns {1, 1, 1}
-- TODO: needs better algorithm
function Color.contrasting(color_hex)
    local h, s, l = Color.rgbhex2hsl(color_hex)

    if l > 128 then l = l - 40 else l = l + 40 end
    
    l = xmath.clamp(l, 0, 255)

    s = math.max(0, s - 10)

    return { Color.hsl2rgb(h / 360, s / 255, l / 255, 1) }
end

return Color
