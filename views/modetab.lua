local style = require("style")
local color = require("color")
local xmath = require("xmath")
local command = require("command")
local undoredo = require("undoredo")
local struct = require("struct")
local icons = require("icons")

local Widget = require("ui.widget")
local Rect = require("rect")
local ImageButton = require("ui.image_button")

local ModeView = Widget:extend("ModeView")

function ModeView:new(color_count)
    self.super.new(self)
    self.selected_mode = 1
    self.background = { color.str2rgb "#202020" }

    self.edit_mode_button = ImageButton(icons.edit_mode)
    self.gallery_mode_button = ImageButton(icons.gallery_mode)

    self.edit_mode_button.is_toggle = true
    self.edit_mode_button.is_active = true
    self.edit_mode_button.on_click_callback = function()
        self.gallery_mode_button.is_active = false
        -- TODO: select edit mode
    end

    self.gallery_mode_button.is_toggle = true
    self.gallery_mode_button.on_click_callback = function()
        self.edit_mode_button.is_active = false
        -- TODO: select gallery mode
    end

end

function ModeView:set_region(region)
    self.super.set_region(self, region)

    self.edit_mode_button:set_region(Rect(region.x, region.y, region.w, region.w))
    self.gallery_mode_button:set_region(Rect(region.x, region.y + region.w + 5, region.w, region.w))
end

function ModeView:update()
    self.super.update(self)
    self.edit_mode_button:update()
    self.gallery_mode_button:update()
end

function ModeView:draw()
    self.super.draw(self)

    self.edit_mode_button:draw()
    self.gallery_mode_button:draw()
end

return ModeView
