local style = require("style")
local msgbus = require("msgbus")
local struct = require("struct")
local ui = require("ui")

local Widget = require("ui.widget")

local ImageView = Widget:extend("ImageView")

function ImageView:new()
    self.super.new(self)

    self.image = nil
    self.offset = { 0, 0 }
    self.scale = 1
    self.width = 0
    self.height = 0

    self.scaleStep = 0
    self.dragStartMouse = { 0, 0 }
    self.dragStartOffset = { 0, 0 }
    self.dragging = false

    self.oldCursor = love.mouse.getCursor()

    self.color_picking = false

    self.tmp_color = { 0, 0, 0, 1 }
    self.selected_color = { 0, 0, 0, 1 }

    self.backdrop = love.graphics.newImage("data/icon_backdrop.png")
    self.backdrop_size = { self.backdrop:getWidth(), self.backdrop:getHeight() }
    self.backdrop_hint_font = love.graphics.newFont("data/TAHOMA.TTF", 14)
    self.backdrop_hint = "Drop an image to begin"
    self.backdrop_hint_size = {
        self.backdrop_hint_font:getWidth(self.backdrop_hint), self.backdrop_hint_font:getHeight(),
    }
end

function ImageView:draw()
    self.super.draw(self)
    love.graphics.setScissor(self.region:unpack())

    love.graphics.setColor(style.background_editor)
    love.graphics.rectangle("fill", self.region:unpack())

    love.graphics.setColor({ 1, 1, 1, 1 })
    if self.image ~= nil then
        love.graphics.draw(self.image, self.region.x + self.offset[1], self.region.y + self.offset[2], 0, self.scale,
                           self.scale)
    else
        love.graphics.draw(self.backdrop, self.region.x + (self.region.w - self.backdrop_size[1]) / 2,
                           self.region.y + (self.region.h - 2 * self.backdrop_size[2]) / 2)

        love.graphics.setFont(self.backdrop_hint_font)
        love.graphics.setColor({ 0.36, 0.36, 0.36, 1.0 })
        love.graphics.print(self.backdrop_hint, self.region.x + (self.region.w - self.backdrop_hint_size[1]) / 2,
                            self.region.y + (self.region.h - self.backdrop_hint_size[2]) / 2 + 40)
    end

    love.graphics.setScissor()

    if self.color_picking then self:draw_color_picker() end
end

function ImageView:draw_color_picker()
    love.graphics.push()

    local offset = { 15, 15 }

    love.graphics.translate(love.mouse.getPosition())

    love.graphics.setColor({ 144.0 / 255, 139.0 / 255, 160.0 / 255 })
    love.graphics.rectangle("fill", offset[1], offset[2], 17, 37)

    love.graphics.setColor(self.tmp_color)
    love.graphics.rectangle("fill", offset[1] + 1, offset[2] + 1, 17 - 2, 37 - 2)

    love.graphics.pop()
end

function ImageView:get_pixel_screen(x, y)

    local imx = (x - self.offset[1] - self.region.x) / self.scale
    local imy = (y - self.offset[2] - self.region.y) / self.scale

    return self:get_pixel_image(imx, imy)
end

function ImageView:get_pixel_image(x, y)
    if x < 0 or x >= self.width or y < 0 or y >= self.height or self.imagedata == nil then return { 0, 0, 0, 1 } end

    return { self.imagedata:getPixel(x, y) }
end

function ImageView:zoom(direction)
    if direction == 0 then return end

    local scaleDelta = 1.0

    local mx, my = love.mouse.getPosition()
    local mx_nrm, my_nrm = self:screen_to_normalized(mx, my) -- mouse in 0..1

    if direction > 0 then
        self.scaleStep = self.scaleStep + 1
        scaleDelta = 1.25
    else
        self.scaleStep = self.scaleStep - 1
        scaleDelta = 0.8
    end

    if self.scaleStep > 0 then
        self.scale = 1 * math.pow(1.25, self.scaleStep)
    else
        self.scale = 1 * math.pow(0.8, math.abs(self.scaleStep))
    end

    local anchor_screen_x, anchor_screen_y = self:normalized_to_screen(mx_nrm, my_nrm)

    self.offset[1] = self.offset[1] + mx - anchor_screen_x
    self.offset[2] = self.offset[2] + my - anchor_screen_y

    self:constrain_to_view()
end

function ImageView:constrain_to_view()
    if self.region == nil then return end

    if self.offset[1] > self.region.w / 2 then self.offset[1] = self.region.w / 2 end

    if self.offset[1] < -self.width * self.scale + self.region.w / 2 then
        self.offset[1] = -self.width * self.scale + self.region.w / 2
    end

    if self.offset[2] > self.region.h / 2 then self.offset[2] = self.region.h / 2 end

    if self.offset[2] < -self.height * self.scale + self.region.h / 2 then
        self.offset[2] = -self.height * self.scale + self.region.h / 2
    end

end

function ImageView:screen_to_normalized(x, y)
    local imx = (x - self.offset[1] - self.region.x) / self.scale / self.width
    local imy = (y - self.offset[2] - self.region.y) / self.scale / self.height
    return imx, imy
end

function ImageView:screen_to_texture(x, y)
    local imx = (x - self.offset[1] - self.region.x) / self.scale
    local imy = (y - self.offset[2] - self.region.y) / self.scale
    return imx, imy
end

function ImageView:normalized_to_screen(x, y)
    local sx = x * self.width * self.scale + self.offset[1] + self.region.x
    local sy = y * self.height * self.scale + self.offset[2] + self.region.y

    return sx, sy
end

function ImageView:update()
    self.super.update(self)

    if self.has_mouse_hover then
        if ui.is_mouse_pressed(1) then
            love.mouse.setCursor(love.mouse.getSystemCursor("crosshair"))
            self.color_picking = true
        elseif ui.is_mouse_released(1) then
            love.mouse.setCursor(love.mouse.getSystemCursor("arrow"))
            if self.color_picking then
                self.color_picking = false
                msgbus.send("image_view.pick_color", self, { from = self.selected_color, to = self.tmp_color })
            end
        end
        self:zoom(ui.mouse_wheel[2])
    end

    if not self.dragging and ui.mouse_down[3] then
        self.dragging = true
        self.dragStartMouse = { love.mouse.getPosition() }
        self.dragStartOffset = self.offset

        self.oldCursor = love.mouse.getCursor()
        love.mouse.setCursor(love.mouse.getSystemCursor("crosshair"))
    end

    if self.dragging and not ui.mouse_down[3] then
        self.dragging = false
        love.mouse.setCursor(self.oldCursor)
    end

    if self.dragging then
        mouse = { love.mouse.getPosition() }
        self.offset = {
            self.dragStartOffset[1] - self.dragStartMouse[1] + mouse[1],
            self.dragStartOffset[2] - self.dragStartMouse[2] + mouse[2],
        }
        print(self.offset[1], self.offset[2])
    end

    if self.color_picking then self.tmp_color = self:get_pixel_screen(love.mouse.getX(), love.mouse.getY()) end

    self:constrain_to_view()
end

function ImageView:center_on_view()
    self.offset[1] = (self.region.w - self.width * self.scale) / 2
    self.offset[2] = (self.region.h - self.height * self.scale) / 2
end

function ImageView:fit_to_view()
    while self.width * self.scale > self.region.w or self.height * self.scale > self.region.h do self:zoom(-1) end
    while self.width * self.scale * 1.25 < self.region.w and self.height * self.scale * 1.25 < self.region.h do
        self:zoom(1)
    end
end

function ImageView:load_from_path(path)
    self.file_path = path
    local file = io.open(path, "rb")
    self:load_from_binary(file:read("*a"))
    file:close()
end

function ImageView:load_from_binary(binary_data)
    local file_data = love.filesystem.newFileData(binary_data, "")
    self.file_contents = binary_data
    self.imagedata = love.image.newImageData(file_data)
    self.image = love.graphics.newImage(self.imagedata)
    self.image:setFilter("nearest", "nearest")
    self.width = self.image:getWidth()
    self.height = self.image:getHeight()
    self.scale = 1
    self.scaleStep = 0
    self:fit_to_view()
    self:center_on_view()
end

function ImageView:save()
    return struct.pack("Lc0", string.len(self.file_contents), self.file_contents)
end

function ImageView:load(data, offset)
    local len = struct.unpack("L", data, offset)
    local file_contents = struct.unpack("c" .. len, data, offset + 8)
    self:load_from_binary(file_contents)
    return offset + 8 + len
end

return ImageView
