local msgbus = require("msgbus")

local Widget = require("ui.widget")
local Histogram = require("ui.histogram")
local Label = require("ui.label")
local VerticalLayout = require("ui.vertical_layout")
local Rect = require("rect")
local Spacer = require("ui.spacer")
-- local Spacer = require("ui.spacer")

local Properties = Widget:extend("Properties")

function identity_histogram(x)
    return x, {x, x, x, 1}
end

function Properties:new()
    self.super.new(self)

    self.hue_histogram = Histogram(identity_histogram)
    self.light_histogram = Histogram(identity_histogram)

    self.layout = VerticalLayout()
    self.layout.insets = Rect(2, 2, 2, 2)

    -- self.layout:add_widget(Label("Hue Histogram"))
    -- self.layout:add_widget(self.hue_histogram)
    -- self.layout:add_widget(Spacer(4))
    -- self.layout:add_widget(Label("Light Histogram"))
    -- self.layout:add_widget(self.light_histogram)
    -- self.layout:add_widget(Label("Color Palettes"))
    -- self.layout:add_widget(PaletteListItem())
    -- self.layout:add_widget(PaletteListItem())
    -- self.layout:add_widget(PaletteListItem())
    -- self.layout:add_widget(Button("Add"))

    msgbus.register_callback(self, self.on_message)
end

function Properties:on_message(msgid, msg)
    print("Properties:on_message", msgid, msg)
end

return Properties
