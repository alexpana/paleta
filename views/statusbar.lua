local Widget = require("ui.widget")

local ui = require("ui")
local style = require("style")

local StatusBar = Widget:extend("StatusBar")

function StatusBar:new()
    self.super.new(self)

    self.color = style.foreground_statusbar
    self.font = style.font

    self.precision = 1.0
end

function StatusBar:draw()
    love.graphics.setColor(self.color)
    love.graphics.setFont(self.font)

    -- TODO: cells!

    love.graphics.print(string.format("Precision: %.2f%%", self.precision), self.region.x + self.region.w - 250, self.region.y + 6)

    love.graphics.print(string.format("FPS: %d", love.timer.getFPS()), self.region.x + self.region.w - 150, self.region.y + 6)

    love.graphics.print(string.format("Version: 0.0.1"), self.region.x + self.region.w - 90, self.region.y + 6)
end

return StatusBar
