local style = require("style")
local color = require("color")
local xmath = require("xmath")
local command = require("command")
local msgbus = require("msgbus")
local struct = require("struct")
local ui = require("ui")
local library = require("library")
local animation = require("ui.animation")

local Widget = require("ui.widget")
local Palette = require("objects.palette")

local PaletteView = Widget:extend("PaletteView")

function PaletteView:new(color_count)
    self.super.new(self)

    self.palette_count = 1
    self.palette = Palette({
        { 0.858824, 0.705882, 0.396078 }, { 0.552941, 0.588235, 0.364706 }, { 0.188235, 0.266667, 0.262745 },
        { 0.129412, 0.168627, 0.137255 }, { 0.419608, 0.368627, 0.196078 }, { 0.164706, 0.203922, 0.172549 },
    })
    self.highlight_heights = { 0, 0, 0, 0, 0, 0 }
    self.selected = 1
    self.color_count = 6

    msgbus.register_callback(self, self.on_message)
end

-- TODO: move this to a common location
function PaletteView.draw_color(x, y, w, h, col, highlight_height)
    local color_hex = color.rgb2hex(unpack(col))

    local contrasting = color.contrasting(color_hex)

    x = math.floor(x)
    y = math.floor(y)
    w = math.floor(w)
    h = math.floor(h)

    love.graphics.push()
    love.graphics.setColor(col)
    love.graphics.rectangle("fill", x, y, w, h)

    label = string.format("#%02x%02x%02x", col[1] * 255, col[2] * 255, col[3] * 255)

    local label_offset = 6

    love.graphics.setFont(style.font_bold)

    love.graphics.setColor(contrasting)
    love.graphics.print(label, math.floor(x) + label_offset, math.floor(y) + label_offset)

    love.graphics.setColor(contrasting)
    love.graphics.print(string.format("%d %d %d", color.rgbhex2hsl(color_hex)), math.floor(x) + label_offset,
                        math.floor(y) + label_offset + 12)

    -- if highlight then
    -- local highlight_height = self.highlight_heights[index]
    love.graphics.setColor(contrasting)
    love.graphics.rectangle("fill", x, y + h - highlight_height, w, highlight_height)
    -- end

    love.graphics.pop()
end

function PaletteView:draw()
    self.super.draw(self)

    local y = 0
    local my_height = self.region.h
    local cell_width = math.floor(self.region.w / self.palette:size())

    for i, col in pairs(self.palette.colors) do
        local width = cell_width
        if i == #self.palette.colors then width = self.region.w - (i - 1) * cell_width end
        PaletteView.draw_color(self.region.x + (i - 1) * cell_width, self.region.y, width, my_height, col, self.highlight_heights[i])
    end
end

function PaletteView:region_under_point(x)
    return xmath.clamp(math.floor((x - self.region.x) / (self.region.w / self.palette:size())) + 1, 1, self.color_count)
end

function PaletteView:swap_colors(i1, i2)
    self.palette:swap_colors(i1, i2)
end

function PaletteView:move_index(from, to)
    local offset = 0
    if from < to then
        offset = 1
    else
        offset = -1
    end

    local n = from
    while n ~= to do
        self:swap_colors(n, n + offset)
        n = n + offset
    end
end

function PaletteView:hovered_index()
    local x = love.mouse.getPosition()
    return self:region_under_point(x)
end

function PaletteView:select_index(index)
    self.selected = self:region_under_point(x)
end

function PaletteView:set_color(col)
    self.palette:set_color(self.selected, col)
    library.set_colors(self.palette)
end

function PaletteView:get_color()
    return self.palette.colors[self.selected]
end

function PaletteView:save()
    local bytes = ""
    return self.palette.serialize_binary()
end

function PaletteView:load(data, offset)
    local offset = self.palette.deserialize_binary(data, offset)
    return offset
end

function PaletteView:update()
    self.super.update(self)

    local x = ui.mouse_position[1]
    local y = ui.mouse_position[2]

    if self.dragging then
        local hovered_index = self:hovered_index()
        if hovered_index ~= self.selected then
            self:swap_colors(self.selected, hovered_index)
            self.selected = hovered_index
        end
    end

    if self.has_mouse_hover and ui.is_mouse_pressed(1) then
        msgbus.send("palette.set_index", self, { from = self.selected, to = self:hovered_index() })
        self.dragging = true
        self.drag_from = self.selected
    end

    if ui.is_mouse_released(1) then
        print("Mouse Released")
        if self.dragging and self.drag_from ~= self.selected then
            msgbus.send("palette.move_slot", self, { from = self.drag_from, to = self.selected })
        end
        self.dragging = false
    end

    for i = 1, 6 do
        local target = 0
        if self.selected == i then target = 16 end
        self.highlight_heights[i] = animation.lerp_to(self.highlight_heights[i], target, 0.5)
    end
end

function PaletteView:on_message(message)
    if message.name == "palette.set_index" then
        self.selected = message.to
    elseif message.name == "palette.move_slot" then
        if message.src ~= self then self:move_index(message.from, message.to) end
    elseif message.name == "image_view.pick_color" then
        self:set_color(message.to)
    end
end

return PaletteView
