function love.conf(t)
    -- t.console = true

    t.window.width = 800
    t.window.height = 800
    t.window.resizable = true
    t.window.vsync = false
    t.window.title = "Paleta"

    t.window.icon = "data/icon.png"

	t.modules.joystick = false
	t.modules.audio = false
	t.modules.sound = false
	t.modules.physics = false
end
