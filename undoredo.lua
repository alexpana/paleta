local undoredo = {}

undoredo.do_functions = {}
undoredo.undo_functions = {}
undoredo.action_history = {}
undoredo.history_index = 0

function undoredo.print_stack()
-- TODO: print action stack for debug purposes
end

function undoredo.perform(action, ...)
    local action_fn = undoredo.do_functions[action]
    if action_fn ~= nil then
        local state = action_fn(...)

        -- don't perform action if state is nil
        if state == nil then return end

        local args = { ... }

        undoredo.history_index = undoredo.history_index + 1
        undoredo.action_history[undoredo.history_index] = { action, args, state }
        undoredo.action_history[undoredo.history_index + 1] = nil
    end
end

-- action already happened, no need to call "do" again
function undoredo.performed(action, args, state)
    local action_fn = undoredo.do_functions[action]
    if action_fn ~= nil then
        undoredo.history_index = undoredo.history_index + 1
        undoredo.action_history[undoredo.history_index] = { action, args, state }
        undoredo.action_history[undoredo.history_index + 1] = nil
    else
        print("undoredo.performed: unknown action " .. action)
    end
end

function undoredo.undo()
    if undoredo.history_index > 0 then
        local action = undoredo.action_history[undoredo.history_index]
        undoredo.undo_functions[action[1]](unpack(action[3]))
        undoredo.history_index = undoredo.history_index - 1
    end
end

function undoredo.redo()
    if undoredo.history_index < #undoredo.action_history then
        undoredo.history_index = undoredo.history_index + 1
        local action = undoredo.action_history[undoredo.history_index]
        undoredo.do_functions[action[1]](unpack(action[2]))
    end
end

function undoredo.register_action(name, functions)
    undoredo.do_functions[name] = functions[1]
    undoredo.undo_functions[name] = functions[2]
end

return undoredo
